<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Offer;
use App\Enterprise;

class OfferTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create("es_ES");

        $enterprises = Enterprise::all();

        foreach ($enterprises as $enterprise) {
            for ($i=0; $i < $faker->randomElement([0,1,2,3]); $i++) { 
                $offer_opts = [
                    "requirements"=>$faker->paragraph(),
                    "recommended"=>$faker->paragraph(),
                    "description"=>$faker->paragraph(),
                    "title"=>$faker->text(255),
                    "work_day"=>$faker->randomElement(['full day','half day']),
                    "schedule"=>$faker->randomNumber(2),
                    "contract"=>$faker->randomElement(['FCT','Practice','Temporay','Indefinite']),
                    "salary"=>$faker->randomNumber(4),
                    "status"=>$faker->randomElement(['Pend_Validacion','Pend_Confirmacion','Pausada','Finalizada','Denegada']),
                    "student_number"=>$faker->randomNumber(2),
                    "start_date"=>$faker->date(),
                    "end_date"=>$faker->date()
                ];

                $enterprise->offers()->create($offer_opts);
            }

        }

    }
}
