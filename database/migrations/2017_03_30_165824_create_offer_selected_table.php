<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferSelectedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_selected', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_offer');
            //$table->foreign('id_offer')
              //  ->references('id')->on('offers')->onDelete('cascade');
            $table->integer('id_student');
            //$table->foreign('id_student')
              //  ->references('id')->on('students')->onDelete('cascade');
            $table->integer('id_teacher');
            //$table->foreign('id_teacher')
              //  ->references('id')->on('teachers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_selected');
    }
}
