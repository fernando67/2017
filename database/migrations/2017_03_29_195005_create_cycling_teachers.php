<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyclingTeachers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycling_teachers', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('id_profesor')->unsigned(); //FK
            $table->integer('id_cicloFormativo')->unsigned(); //FK
            $table->string('promocion');
            $table->timestamps();    //Añade create_at y update_at

            $table->foreign('id_profesor')->references('id')->on('teachers')->onDelete('cascade');
            $table->foreign('id_cicloFormativo')->references('id')->on('formativeCycles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('cycling_teachers');
    }
}
