<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_offer');
            //$table->foreign('id_offer')
              //  ->references('id')->on('offers')->onDelete('cascade');
            $table->integer('id_student');
            //$table->foreign('id_student')
              //  ->references('id')->on('students')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_subscriptions');
    }
}
