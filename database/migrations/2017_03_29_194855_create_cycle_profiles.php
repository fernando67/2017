<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycle_profiles', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('id_cicloFormativo')->unsigned(); //FK
            $table->string('nombre');
            $table->timestamps();    //Añade create_at y update_at

            $table->foreign('id_cicloFormativo')->references('id')->on('formativeCycles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('cycle_profiles');
    }
}
