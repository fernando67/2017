<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentCoursesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('student_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('students');
            //  nombre del ciclo formativo en ingles
           // $table->integer('cicloformativo_id')->unsigned();
            //  nombre de la tabla en ingles
           // $table->foreign('cicloformativo_id')->references('id')->on('ciclosformativos');
           // No puedes usar 2 timestamp 
            $table->dateTime("fecha_inicio");
           $table->dateTime("fecha_fin");
            $table->string('promocion', 20);
            $table->boolean('finalizado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('student_courses');
    }

}
