<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormativeCycles extends Migration
{
    public function up() {
        Schema::create('formativeCycles', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('id_familiaProfesional')->unsigned(); //FK
            $table->enum('tipo',['CFGS','CFGM','FPB']);
            $table->enum('plan',['LOE','LOGSE','LOMCE']);
            $table->string('nombre');
            $table->timestamps();    //Añade create_at y update_at

            $table->foreign('id_familiaProfesional')->references('id')->on('professional_families')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::drop('formativeCycles');
    }
}