<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProfessionalFamilys extends Migration
{
    public function up() {
        Schema::create('professional_families', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();    //Añade create_at y update_at
        });
    }

    public function down() {
        Schema::drop('professional_families');
    }
}
