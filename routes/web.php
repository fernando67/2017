<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name("index");

Auth::routes();

Route::get('/home', ["as" => "home" , "uses" => 'HomeController@index',"middleware" => "auth" ]);


// RUTAS PARA EL REGISTRO

    //**ALUMNO**
Route::post("student/store",["as" => "student.store", "uses" => "StudentController@store"]);

    // **PROFESOR**
Route::post("teacher/store",["as" => "teacher.store", "uses" => "TeacherController@store"]);

Route::resource('offers','OfferController');
    // **EMPRESA**
Route::post("enterprise/store",["as" => "enterprise.store", "uses" => "EnterpriseController@store"]);

Route::get("user/{user}",["as"=>"user.profile","uses"=> "UserController@profile"]);   //**************************

Route::group(["prefix" => "admin"], function () {



    Route::put("user/toggle/active/{user}",["as"=>"user.toggle.active","uses"=> "UserController@changeActive"]);
    Route::post("user/toggle/active/selected",["as"=>"user.toggle.selected","uses"=> "UserController@changeSelected"]);
    Route::get("user/inactive",["as"=>"user.inactive","uses"=> "UserController@inactive"]);

    Route::get("teacher/inactive",["as"=>"teacher.inactive","uses"=> "TeacherController@inactive"])->middleware("rol:is_admin");

    Route::get("student/inactive",["as"=>"student.inactive","uses"=> "StudentController@inactive"])->middleware("rol:is_admin|is_teacher");

    Route::get("enterprise/inactive",["as"=>"enterprise.inactive","uses"=> "EnterpriseController@inactive"])->middleware("rol:is_admin|is_teacher");

    //Route::resource('enterprise', 'EnterpriseController');

    Route::get("student/create",["as" => "student.create", "uses" => "StudentController@create"])->middleware("rol:is_admin|is_teacher");
    Route::get("teacher/create",["as" => "teacher.create", "uses" => "TeacherController@create"])->middleware("rol:is_admin");
    Route::get("enterprise/create",["as" => "enterprise.create", "uses" => "EnterpriseController@create"])->middleware("rol:is_admin|is_teacher");


    Route::get("enterprise",["as" => "enterprise.index", "uses" => "EnterpriseController@index"])->middleware("rol:is_admin|is_teacher");
    Route::get("enterprise/{enterprise}",["as" => "enterprise.show", "uses" => "EnterpriseController@show"])->middleware("rol:is_teacher","OneSelf");
    Route::get("enterprise/{enterprise}/edit",["as" => "enterprise.edit", "uses" => "EnterpriseController@edit"])->middleware("rol:is_admin|is_teacher","OneSelf");
    Route::delete("enterprise/{enterprise}",["as" => "enterprise.destroy", "uses" => "EnterpriseController@destroy"])->middleware("rol:is_admin","OneSelf");
    Route::put("enterprise/{enterprise}",["as" => "enterprise.update", "uses" => "EnterpriseController@update"])->middleware("rol:is_admin|is_teacher","OneSelf");

    //Route::resource('student', 'StudentController');

    Route::get("student",["as" => "student.index", "uses" => "StudentController@index"])->middleware("rol:is_admin|is_teacher");
    Route::get("student/{student}",["as" => "student.show", "uses" => "StudentController@show"])->middleware("rol:is_admin|is_teacher","OneSelf");
    Route::get("student/{student}/edit",["as" => "student.edit", "uses" => "StudentController@edit"])->middleware("OneSelf");
    Route::delete("student/{student}",["as" => "student.destroy", "uses" => "StudentController@destroy"])->middleware("rol:is_admin","OneSelf");
    Route::put("student/{student}",["as" => "student.update", "uses" => "StudentController@update"])->middleware("rol:is_admin","OneSelf");



    //Route::resource('teacher', 'TeacherController');

    Route::get("teacher",["as" => "teacher.index", "uses" => "TeacherController@index"])->middleware("rol:is_admin|is_teacher");
    Route::get("teacher/{teacher}",["as" => "teacher.show", "uses" => "TeacherController@show"])->middleware("rol:is_admin|is_teacher","OneSelf");
    Route::get("teacher/{teacher}/edit",["as" => "teacher.edit", "uses" => "TeacherController@edit"])->middleware("OneSelf");
    Route::delete("teacher/{teacher}",["as" => "teacher.destroy", "uses" => "TeacherController@destroy"])->middleware("rol:is_admin","OneSelf");
    Route::put("teacher/{teacher}",["as" => "teacher.update", "uses" => "TeacherController@update"])->middleware("rol:is_admin","OneSelf");



});
/*
Route::group(["prefix" => "admin",'middleware' => ['rol:is_teacher|is_admin']], function () {
    Route::resource('students', 'StudentController');
});

Route::group(["prefix" => "admin",'middleware' => ['rol:is_teacher|is_admin']], function () {
    Route::resource('teacher', 'TeacherController');
});
*/
Route::group(["prefix" => "admin",'middleware' => ['rol:is_admin']],function(){
    Route::resource('family', 'ProfessionalFamilyController');
});
