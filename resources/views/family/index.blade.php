@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{route("family.create")}}" class="btn btn-info pull-left">Nueva Familia</a>
                        <form class="form-inline col-md-offset-8">

                            <!-- Input con el término de la búsqueda -->
                            <input type="text" class="form-control" id="inlineFormInput" name="nombre" placeholder="Ej: Informática">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </form>
                    </div>
                    <div class="panel-body">
                        @include('family.partials.table')
                    </div>
                    <div class="panel-footer">{{$profesionalfamilys->appends($request->only(['nombre']))->render()}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection