@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 align="center">{{$family->nombre}}</h2>
                        <a href="{{route("family.update",$family->id)}}" class="btn btn-info">Editar</a>
                    </div>
                    <div class="panel-body">

                        <h3 align="center">Aquí aparecerían</h3>
                        <h4 align="center">los Ciclos Formativos vinculados </h4>
                        <h5 align="center">a esta familia </h5>

                    </div>

                    <div class="panel-footer">
                        <a class="btn btn-secondary pull-left" href="{{route("family.index")}}">Ir al Index</a>
                        <h5 align="right"><sup>Creado el: {{$family->created_at}}</sup></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection