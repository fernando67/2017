<table class="table table-hover ">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Created</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    @foreach($profesionalfamilys as $profesionalfamily)
        <tr>
            <td>
                {{$profesionalfamily->id}}
            </td>
            <td>
                <a href="{{route("family.show",$profesionalfamily->id)}}">{{$profesionalfamily->nombre}}</a>
            </td>
            <td>
                {{$profesionalfamily->created_at->diffForHumans()}}
            </td>
            <td>
                <a href="{{route("family.edit",$profesionalfamily->id)}}">Editar</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['family.destroy', $profesionalfamily->id]]) !!}
                        <button href="#" class="btn btn-link" type="submit">Eliminar</button>
                {!! Form::close() !!}

            </td>
        </tr>
        @endforeach
</table>