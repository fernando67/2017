@if (Session::has('message'))
    <div class="alert alert-success">
        <strong>{{ Session::get('message') }}</strong>
    </div>
@endif