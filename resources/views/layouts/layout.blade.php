<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bolsa de Trabajo || IES Ingeniero de la Cierva</title>
    <!-- AQUI EL CSS -->
    @include("layouts.partials.css")
</head>

<body class="nav-sm">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">

                    <a href="index.html" class="site_title"><i class="fa fa-rocket"></i> <span>Bolsa de Trabajo</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="{{asset("images/" . str_replace('is_', '', Auth::user()->rol) . ".png")}}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bienvenido,</span>
                        <h2>{{auth()->user()->name}}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                    @include("layouts.partials.menu")
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a class="fullScreen-button" data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    {{ Form::open(['route' => 'logout', 'method' => 'post',"id"=>"logout-form"]) }}
                    <a class="logout-button" type="submit" data-toggle="tooltip" data-placement="top" title="Logout" href="#">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                    {{Form::close()}}
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{asset("images/" . str_replace('is_', '', Auth::user()->rol) . ".png")}}" alt="">{{ Auth::user()->name }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li>
                                    <a href="{{route("user.profile",auth()->user()->id)}}">Perfil</a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a class="logout-button" href="#"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                        {!! notificationCenter() !!}

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" >
            @include("feedback.errors")
            @include("feedback.feedback")
            @yield("css")
            @yield("content")
        </div>

        <style>
            .right-col[role="main"]{
                min-height: 815px;
            }
        </style>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right" id="div-love-heart">
               hecha con <span class="fa fa-heart animated pulse infinite" id="love-heart"></span> por <b><a href="https://bitbucket.org/bolsadetrabajoiescierva/profile/members">Bolsa de Trabajo Team&#8482;</a></b>
            </div>
            <div class="clearfix">Una aplicación de <b><a href="http://www.iescierva.net"> IES Ingeniero de la Cierva </a></b></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<!-- AQUI LOS SCRIPTS -->
@include("layouts.partials.js")
</body>
</html>

