<table class="table table-hover " id="table-data">
    <thead>
    <tr>
        <th>#</th>
        <th>Empresa</th>
        <th>Titulo</th>
        <th>Jornada</th>
        <th>Horario</th>
        <th>Salario</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    @foreach($offers as $offer)
        <tr>
            <td>
                {{$offer->id}}
            </td>
            <td>
                {{$offer->enterprise->user->name}} ({{$offer->enterprise->sociedad}})
            </td>
            <td>
                <a href="{{route("offers.show",$offer->id)}}">{{$offer->title}}</a>
            </td>
            <td>
                {{$offer->work_day}}
            </td>
            <td>
                {{$offer->schedule}}
            </td>
            <td>
                {{$offer->salary}}
            </td>

            <td>
                <a href="{{route("offers.edit",$offer->id)}}"><button href="#" class="btn btn-link">Editar</button></a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['offers.destroy', $offer->id]]) !!}
                <button href="#" class="btn btn-link" type="submit">Eliminar</button>
                {!! Form::close() !!}
            </td>
        </tr>

    @endforeach
</table>