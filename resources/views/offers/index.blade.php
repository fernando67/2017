@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{route("offers.create")}}" class="btn btn-info pull-left">Nueva Oferta</a>
                        <form class="form-inline col-md-offset-7">
                            <!-- Select con el método de búsqueda-->
                            <select class="form-control" id="sel1" name="method" required>
                                <option value="email">Email</option>
                                <option value="nombre">Nombre</option>
                                <option value="apellidos">Apellidos</option>
                                <option value="nrp">NRP</option>
                                <option value="telefono">Teléfono</option>
                            </select>
                            <!-- Input con el término de la búsqueda -->
                            <input type="text" class="form-control" id="inlineFormInput" name="search" placeholder="">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </form>
                    </div>
                    <div class="panel-body">
                        @include('offers.partials.table')
                    </div>
                    {{$offers->appends(Request::except('page'))->render()}}
                </div>
            </div>
        </div>
    </div>
@endsection