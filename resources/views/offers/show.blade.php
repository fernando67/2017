@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="col-md-offset-5"><h3>Datos de Oferta</h3></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tr>
                                <td>
                                    Titulo:
                                </td>
                                <td>
                                    {{$offer->title}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Descripcion:
                                </td>
                                <td>
                                    {{$offer->description}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Requisitos:
                                </td>
                                <td>
                                    {{$offer->requirements}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Recomendados:
                                </td>
                                <td>
                                    {{$offer->recommended}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Jornada:
                                </td>
                                <td>
                                    {{$offer->work_day}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Horario:
                                </td>
                                <td>
                                    {{$offer->schedule}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contrato:
                                </td>
                                <td>
                                    {{$offer->contract}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Salario:
                                </td>
                                <td>
                                    {{$offer->salary}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Estado:
                                </td>
                                <td>
                                    {{$offer->status}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Numero de alumnos:
                                </td>
                                <td>
                                    {{$offer->student_number}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Fecha Inicio:
                                </td>
                                <td>
                                    {{$offer->start_date}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Fecha Fin:
                                </td>
                                <td>
                                    {{$offer->end_date}}
                                </td>
                            </tr>
                        </table>
                        <div class="form-group pull-left">
                            <a href="{{route("offers.index")}}" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection