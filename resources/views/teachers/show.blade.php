@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($teacher->is_admin == 0)
                            <h3 align="center">Profesor</h3>
                        @else
                            <h3 align="center">Administrador</h3>
                        @endif
                        <h4 align="center"> {{$teacher->user->name}} {{$teacher->apellidos}}</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tr>
                                <td>
                                    Nombre:
                                </td>
                                <td>
                                    {{$teacher->user->name}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Apellidos:
                                </td>
                                <td>
                                    {{$teacher->apellidos}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    NRP/Expediente:
                                </td>
                                <td>
                                    {{$teacher->nrp_expediente}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Telefono:
                                </td>
                                <td>
                                    {{$teacher->user->phone}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email:
                                </td>
                                <td>
                                    {{$teacher->user->email}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Administrador:
                                </td>
                                <td>
                                    @if($teacher->is_admin == 0)
                                        No
                                    @else
                                        Si
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <div class="form-group pull-left">
                            <a href="{{url()->previous()}}" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection