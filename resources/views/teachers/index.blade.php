@extends('layouts.layout') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{route("teacher.create")}}" class="btn btn-info pull-left">Nuevo Profesor</a>
                    <form class="form-inline col-md-offset-7">
                        <!-- Select con el método de búsqueda-->
                        <select class="form-control" id="sel1" name="method" required>
                            <option value="email">Email</option>
                            <option value="nombre">Nombre</option>
                            <option value="apellidos">Apellidos</option>
                            <option value="nrp">NRP</option>
                            <option value="telefono">Teléfono</option>
                        </select>
                        <!-- Input con el término de la búsqueda -->
                        <input type="text" class="form-control" id="inlineFormInput" name="search" placeholder="Ej: cmabris@gmail.com">
                        <button type="submit" class="btn btn-primary">Buscar</button>
                    </form>
                </div>
                <div class="panel-body">
                    @include('teachers.partials.table')
                </div>
                {{--<div class="panel-footer">{{$teachers->appends($request->only(['method',"search"]))->render()}}</div>--}}
            </div>
        </div>
    </div>
</div>
@endsection