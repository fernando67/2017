<div class="form-group">
    {{ Form::label('name',"Nombre") }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder' => "Ej: Pepe"]) }}
</div>

<div class="form-group">
    {{ Form::label('apellidos',"Apellidos") }}
    {{ Form::text('apellidos',null,['class'=>'form-control','placeholder' => "Ej: Sánchez"]) }}
</div>

<div class="form-group">
    {{ Form::label('nrp_expediente',"NRP / Expediente") }}
    {{ Form::text('nrp_expediente',null,['class'=>'form-control','placeholder' => "Ej: 12912193DK"]) }}
</div>

<div class="form-group">
    {{ Form::label('phone',"Telefono") }}
    {{ Form::text('phone',null,['class'=>'form-control','placeholder' => "Ej: 690 253 122"]) }}
</div>

<div class="form-group">
    {{ Form::label('email',"Email") }}
    {{ Form::email('email',null,['class'=>'form-control','placeholder' => "Ej: pepesanchez@gmail.com"]) }}
</div>

<div class="form-group">
    {{ Form::label('password',"Contraseña") }}
    {{ Form::password('password',['class'=>'form-control','placeholder' => "Ej: ******"]) }}
</div>

<div class="form-group">
    {{ Form::label('password_confirmation',"Repetir Contraseña") }}
    {{ Form::password('password_confirmation',['class'=>'form-control','placeholder' => "Repetir Contraseña"]) }}
</div>
@if(auth()->user()->rol == "is_admin")
<div class="form-group">
    {{ Form::label('is_admin',"Admin") }}
    {{ Form::checkbox('is_admin',"false") }}
</div>

@endif