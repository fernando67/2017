@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="col-md-offset-5"><h3>Nuevo Alumno</h3></div>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['route' => ['student.store'],'method' => 'POST']) }}
                        @include('student.partials.fields')
                        {{ Form::submit("Crear",['class' => 'btn btn-warning'])}}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>


@endsection