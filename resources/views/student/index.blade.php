@extends('layouts.layout')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{route("student.create")}}" class="btn btn-info pull-left">Nuevo Alumno</a>
                        <form class="form form-inline col-md-offset-7">
                            <select class="form-control" id="sel1" name="method" required>
                                <option value="name" @php echo ($metodo == "name")? "selected":""; @endphp>Nombre</option>
                                <option value="nre" @php echo ($metodo == "nre")? "selected":""; @endphp>NRE</option>
                                <option value="email" @php echo ($metodo == "email")? "selected":""; @endphp>Email</option>
                                <option value="status" @php echo ($metodo == "status")? "selected":""; @endphp>Estado</option>
                            </select>
                            <!-- Input con el término de la búsqueda -->
                            <input type="text" class="form-control" id="inlineFormInput" name="search" placeholder="Ej: cmabris@gmail.com">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </form>
                    </div>
                    <div class="panel-body">
                        <article class="container">

                            @include('student.partials.table')
                            {{$students->appends(Request::except('page'))->render()}}
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection