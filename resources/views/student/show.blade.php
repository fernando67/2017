@extends('layouts.layout') 

@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 align="center">Alumno</h3>
                        <h4  align="center">{{ $student->user->name }} {{ $student->apellidos }}</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Alumno:</th>
                                <td>{{ $student->user->name }} {{ $student->apellidos }}</td>
                            </tr>
                            <tr>
                                <th>NRE:</th>
                                <td>{{ $student->nre }}</td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td>{{ $student->user->email }}</td>
                            </tr>
                            <tr>
                                <th>Teléfono:</th>
                                <td>{{ $student->user->phone }}</td>
                            </tr>
                            <tr>
                                <th>Vehículo propio:</th>
                                <td>
                                    {{ ($student->vehiculo) ? "Sí" : "No" }}
                                </td>
                            </tr>
                            <tr>
                                <th>Estado:</th>
                                <td>{{ $student->status }}</td>
                            </tr>
                            <tr>
                                <th>Domicilio:</th>
                                <td>{{ $student->domicilio }}</td>
                            </tr>
                            <tr>
                                <th>Edad:</th>
                                <td>{{ $student->edad }}</td>
                            </tr>

                            </table>
                        <div class="form-group pull-left">
                            <a href="{{url()->previous()}}" class="btn btn-default">Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection