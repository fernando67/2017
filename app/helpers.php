<?php

use App\User;
use App\Student;
use App\Teacher;
use App\Enterprise;


function estadisticasUsuarios($opts)
{
//Hacerlo con un swich y que la función lleve un atributo, solo coger esa variable para no tener datos almacenados de forma innecesaria

    switch ($opts) {
        case "users":
            return User::Active(1)->count();
            break;
        case "studentsNumero":
            return Student::Active()->count();
            break;
        case "teachersNumero":
            return Teacher::Active()->IsAdmin(0)->count();
            break;
        case "enterprisesNumero":
            return Enterprise::Active()->count();
            break;
        case "adminsNumero":
            return Teacher::Active()->IsAdmin(1)->count();
            break;
        case "studentsPorcentaje":
            return number_format(Student::Active()->count() * 100 / User::Active(1)->count(), 2);
            break;
        case "teachersPorcentaje":
            return number_format(Teacher::Active()->IsAdmin(0)->count() * 100 / User::Active(1)->count(), 2);
            break;
        case "enterprisesPorcentaje":
            return number_format(Enterprise::Active()->count() * 100 / User::Active(1)->count(), 2);
            break;
        case "adminsPorcentaje":
            return number_format(Teacher::Active()->IsAdmin(1)->count() * 100 / User::Active(1)->count(), 2);
            break;

        case "pendingStudents":
            return Student::NoActive()->count();
            break;

        case "pendingStudentsPorcentaje":
            return number_format(Student::NoActive()->count() * 100 / User::Active(1)->count(), 2);
            break;

        case "pendingEnterprises":
            return Enterprise::NoActive()->count();
            break;

        case "pendingEnterprisesPorcentaje":
            return number_format(Enterprise::NoActive()->count() * 100 / User::Active(1)->count(), 2);
            break;

        case "pendingTeachers":
            return Teacher::NoActive()->count();
            break;

        case "pendingTeachersPorcentaje":
            return number_format(Teacher::NoActive()->count() * 100 / User::Active(1)->count(), 2);
            break;
    }

}

function notificationCenter()
{
    $notificationCenter = "";
    if (auth()->user()->rol == "is_admin" || auth()->user()->rol == "is_teacher") {
        // Empiezo a generar la lista
        $notificationCenter .= '<li role="presentation" class="dropdown">';
        $notificationCenter .= '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">';
        $notificationCenter .= '<i class="fa fa-bell-o "></i>';

        //** CONTADOR TOTAL **//

            $notificationCenter .= notificationCounter();

        //** FIN DE CONTADOR TOTAL **//

            $notificationCenter .= '</a><ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">';

        //** GENERACIÓN DE LOS CONTADORES **//

            if (auth()->user()->rol == "is_admin") {
                //Genera el numero de profesores pendientes de validacion
                $notificationCenter .= notificationCount("teacher");
            }
            //Genera el numero de empresas pendientes de validacion
            $notificationCenter .= notificationCount("enterprise");

            //Genera el numero de alumnos pendientes de validacion
            $notificationCenter .= notificationCount("student");

        //** FIN DE LA GENERACIÓN DE LOS CONTADORES **//


        //** INICIO DE LA GENERACIÓN DE NOTIFICACIONES **//

        $notificationCenter .= '<li><div class="slimScroll"><ul class="list-unstyled msg_list">';

        $notificationCenter .= timeline();

        //** FIN DE LA GENERACIÓN DE NOTIFICACIONES **//

        if (auth()->user()->rol == "is_admin") {
            if (\App\Teacher::NoActive()->count() == 0 && \App\Student::NoActive()->count() == 0 && \App\Enterprise::NoActive()->count() == 0) {
                $notificationCenter .= '<li>No hay notificaciones pendientes</li>';
            }
        } else if (auth()->user()->rol == "is_teacher") {
            if (\App\Student::NoActive()->count() == 0 && \App\Enterprise::NoActive()->count() == 0) {
                $notificationCenter .= '<li>No hay notificaciones pendientes</li>';
            }
        }


        $notificationCenter .= '</ul></div></li><li>
                      <div class="text-center">
                        <a href="'.route("user.inactive").'">
                          <strong>Ver todos los usuarios</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li></ul></li>';
    }

    return $notificationCenter;

}

function timeline()
{
    $pending = "";
    \Carbon\Carbon::setLocale("es");
    foreach (\App\User::Active(0)->orderBy("created_at","desc")->get() as $model) {

        if(auth()->user()->rol == "is_teacher" && $model->rol == "is_teacher"){
            $pending .="";
        }else{
            $pending .= '<li>';
            $pending .= '<a href="' . route('user.profile', $model->id) . '">';
            $pending .= '<span class="image"><img src="' . asset("images/".$model->role.".png") . '" alt="Profile Image" /></span>';
            $pending .= '<span>';
            $pending .= '<span><b>' . $model->name . '</b></span>';
            $pending .= '</span>';
            $pending .= '<span class="message">Este usuario está esperando que lo validen ';
            $pending .= ''.Form::open(['method' => 'PUT', 'route' => ["user.toggle.active", $model->id]]) . '';

            $pending .= '<span class="time" style="position:initial">' . $model->created_at->diffForHumans() . '</span>';
            $pending .= '<button href="#" class="btn btn-success pull-right" type="submit">VALIDAR</button>';
            $pending .= '' . Form::close() . '';
            $pending .= '</span>';
            $pending .= '</a>';
            $pending .= '</li>';
        }

    }

    return $pending;
}


function notificationCounter()
{
    if (auth()->user()->rol == "is_admin") {
        if (\App\Student::NoActive()->count() + \App\Enterprise::NoActive()->count() + \App\Teacher::NoActive()->count() != 0) {

            $count_total = \App\Student::NoActive()->count() + \App\Enterprise::NoActive()->count() + \App\Teacher::NoActive()->count();
            return "<span class='badge bg-green' >" . $count_total . "</span >";
        } 
    }elseif(auth()->user()->rol == "is_teacher"){
            if (\App\Student::NoActive()->count() + \App\Enterprise::NoActive()->count() != 0) {
                $count_total = \App\Student::NoActive()->count() + \App\Enterprise::NoActive()->count();
                return '<span class="badge bg-green" >'.$count_total . '</span >';
            }

        }
}

function notificationCount($type)
{
    $count = "";
    $opts = [];
    switch ($type) {
        case "teacher":
            $opts = ["model" => \App\Teacher::class, "user" => "PROFESORES", "route" => "teacher.inactive"];
            break;

        case "student":
            $opts = ["model" => \App\Student::class, "user" => "ALUMNOS", "route" => "student.inactive"];
            break;

        case "enterprise":
            $opts = [ "model" => \App\Enterprise::class, "user" => "EMPRESAS", "route" => "enterprise.inactive" ];
            break;
    }

    if ($opts["model"]::NoActive()->count() != 0) {
        $count .= '<li><div class="text-center"><a href="' . route($opts["route"]) . '">';
        $count .= "<strong>";

        //CAMBIO EL COLOR DEL LABEL DEPENDIENDO DEL NUMERO DE USUARIOS PENDIENTES TENGA
        if($opts["model"]::NoActive()->count() >= 20){
            $count .= '<span class="label label-danger pull-left">';
        }elseif($opts["model"]::NoActive()->count() >= 10){
            $count .= '<span class="label label-warning pull-left">';
        }elseif ($opts["model"]::NoActive()->count() < 10){
            $count .= '<span class="label label-success pull-left">';
        }

        $count .= ''. $opts["model"]::NoActive()->count() . '</span> ' . $opts["user"] . ' sin validar </strong>';
        $count .= '</a></div></li>';

        return $count;
    } else
        return null;

}



