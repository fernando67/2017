<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model {

    //Inluir Softdeletes

    public $timestamps = false;
    protected $fillable = [
        'apellidos', 'nre', 'vehiculo', 'domicilio', 'status', 'edad',
    ];

    /** RELACIONES **/ //Aquí las relaciones

    //Relacion con user
     public function user()
    {
        return $this->belongsTo('App\User');
    }







    /** SETTERS **/ //Aquí los setters
    public function setVehiculoAttribute($value)
    {
        if (trim($value) != ''){
            $this->attributes['vehiculo'] = $value;
        }else{
            $this->attributes['vehiculo'] = 0;
        }
    }







    /** SCOPES **/ //Aquí los scopes

    public function scopeSearch($query, Request $request)
    {
        //Analizo si tiene método de búsqueda y si tiene el campo search
        if($request->has("method") && $request->has("search")){

            if(trim($request->get("search")) != ''){

                //Asignación del término de búsqueda. El use no permite métodos.
                $search = $request->get("search");


                //Hago un switch con los métodos de búsqueda (Por nombre, apellidos, nrp, etc) y ejecuto el scope con el campo búsqueda
                switch ($request->get("method")){

                    case "name":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function($q) use ($search)
                        {
                            $q->Name($search);

                        });
                        break;

                    case "nre":
                        //usar this->scopeNre
                        $query->where('nre','LIKE',"%".$search."%");

                        break;

                    case "status":
                        //usar this->scopeStatus
                        $query->where('status','=',"$search");

                        break;

                    case "email":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function($q) use ($search)
                        {
                            $q->Email($search);
                        });

                        break;

                }

            }


        }
    }



    //Scope para el apellido del alumno
    /*public function scopeApellido($query,$apellido)
    {

    }

    //Scope para el nre del alumno
    public function scopeNre($query,$nre)
    {

    }

    //Scope para el vehiculo del alumno
    public function scopeVehiculo($query,$vehiculo)
    {

    }
    //Scope para el domicilio del alumno
    public function scopeDomicilio($query,$domicilio)
    {

    }

    //Scope para el status del alumno
    public function scopeStatus($query,$status)
    {

    }*/



    public function scopeDatos($query,$datos) {
        
        //Cambiar
             return $query->orWhere('apellidos', 'like', "%".$datos."%")
                     ->orWhere('domicilio', 'like', "%".$datos."%");
        
    }

    public function scopeNoActive($query)
    {
        $query->with("user")->whereHas('user', function($q)
        {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(0);
        });
    }

    public function scopeActive($query)
    {
        $query->with("user")->whereHas('user', function($q)
        {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(1);
        });
    }
    
    
}
