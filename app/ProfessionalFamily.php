<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProfessionalFamily extends Model
{

    protected $fillable =[
        'nombre',"created_at"
    ];

    /** RELACIONES **/ //Aquí las relaciones






    /** GETTERS **/ //Aquí los getters






    /** SETTERS **/ //Aquí los setters






    /** SCOPES **/ //Aquí los scopes
    public function scopeName($query,Request $request)
    {
        $name = $request->get("nombre");
        if (trim($name) != '') {
            $query->where('nombre','LIKE',"%$name%");
        }
    }

}
