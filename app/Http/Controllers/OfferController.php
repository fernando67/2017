<?php

namespace App\Http\Controllers;

use App\Offer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //cambiar para que busque en la relacion
        $offers = Offer::paginate(10);

        return view('offers.index',compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Crear OFERTA
        $offer = new Offer;
        //id del auth
        $offer->enterprise_id = Auth::user()->enterprise->id;

        $offer->requirements = $request->requirements;
        $offer->recommended = $request->recommended;
        $offer->description = $request->description;
        $offer->title = $request->title;
        $offer->work_day = $request->work_day;
        $offer->schedule = $request->schedule;
        $offer->contract = $request->contract;
        $offer->salary = $request->salary;
        $offer->status = "Pend_Validacion";
        $offer->student_number = $request->student_number;
        $offer->start_date = $request->start_date;
        $offer->end_date = $request->end_date;

        $respuesta = $offer->save();

        //Crear y guardar los modelos de las relaciones
        return redirect()->route("offers.index", $respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        return view('offers.show',compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        return view('offers.edit',compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {

        $offer->fill($request->only(["requirements", "recommended", "title", "work_day", "schedule", "contract", "start_date", "description", "salary", "student_number"]));
        $offer->status = "Pend_Validacion";
        $offer->save();

        return redirect()->route("offers.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();

        return redirect()->route("offers.index" );
    }
}
