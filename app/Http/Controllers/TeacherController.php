<?php

namespace App\Http\Controllers;

/** REQUEST */
use Illuminate\Http\Request;
use App\Http\Requests\TeacherStoreRequest;
use App\Http\Requests\TeacherUpdateRequest;

/** MODELOS */
use App\Teacher;
use App\User;

/**  MAILS*/
use App\Mail\newRegisteredUser;
use Illuminate\Support\Facades\Mail;

/** OTROS */
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Session;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
         * Scope de búsqueda de profesores que estén ACTIVADOS. Organizada por métodos de búsqueda (por email, nrp, telefono).
         * */

        // Crear los Scopes necesarios (los de todos los campos) en el modelo Teacher para evitar repetir código en el futuro.
        $teachers = Teacher::Search($request)->Active()->get();

        return view('teachers.index', compact('teachers','request'));

        //dd($teachers);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherStoreRequest $request)
    {

        //dd($request->only(["email","password","name","phone"]));

        $user = new User($request->only(["email","password","name"]));
        $user->phone = $request->get("phone");
        $user->is_active = 1;
        $user->save();

        Mail::to($user->email)->send(new newRegisteredUser($user));

        $teacher = $user->teacher();


        $opts = [
            "apellidos" => $request->get("apellidos"),
            "nrp_expediente" => $request->get("nrp_expediente"),
            "is_admin" => $request->get("is_admin")
        ];

        $teacher->create($opts);
        Session::flash('message', 'Profesor agregado correctamente');
        return redirect()->route("teacher.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        return view('teachers.show',compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        Session::flash('user_id', $teacher->user->id);
        $user = array_merge($teacher->toArray(),$teacher->user->toArray());

        $teacher = new Collection($user);

        return view('teachers.edit',compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherUpdateRequest $request, Teacher $teacher)
    {
        $teacher->user->fill($request->only(["email","password","name","phone"]));
        $teacher->user->save();
        ($request->get("is_admin"))?$is_admin = 1:$is_admin = 0;
        $opts = [
            "apellidos" => $request->get("apellidos"),
            "nrp_expediente" => $request->get("nrp_expediente"),
            "is_admin" => $request->get("is_admin")
        ];
        $teacher->fill($opts);
        $teacher->save();
        Session::flash('message', 'Profesor editado correctamente');
        return redirect()->route("teacher.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        //Borra el usuario
        $teacher->user()->delete();

        //Borra el profesor
        $teacher->delete();

        return redirect()->route("teacher.index");
    }

    public function inactive()
    {
        $teachers = Teacher::NoActive()->get();
        return view("teachers.inactive", compact("teachers"));
    }
    
}
