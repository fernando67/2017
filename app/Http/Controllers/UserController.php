<?php

namespace App\Http\Controllers;

use App\Mail\ValidationOk;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\User;
use App\Student;
use App\Teacher;
use App\Enterprise;
use App\TeacherValidation;

class UserController extends Controller
{
    public function inactive()
    {
        $students = Student::NoActive()->get();
        $teachers = Teacher::NoActive()->get();
        $enterprises = Enterprise::NoActive()->get();
        $allValidations = TeacherValidation::orderBy("created_at","desc")->take(10)->get();
        $currentUserValidations = auth()->user()->teacher->validations->take(10);
        return view("inactive.index",compact("students","teachers","enterprises","allValidations","currentUserValidations"));
    }
    public function changeActive(User $user) {

        //Si el usuario esta activo
        if ($user->rol == "is_teacher" && auth()->user()->rol == "is_teacher"){
            return redirect()->back();
        }else {
            if ($user->is_active == 1) {

                //Actualizo su active y borro el registro de validations
                $user->is_active = 0;
                $user->save();
                $user->validatedBy()->create(["teacher_id" => auth()->user()->teacher->id, "user_id" => $user->id, "action" => "DEL"]);
            } else {

                //Creo el registro de validations
                $user->validatedBy()->create(["teacher_id" => auth()->user()->teacher->id, "user_id" => $user->id, "action" => "ADD"]);

                //Por aquí el campo action para la tabla validate

                $user->validatedBy->save();
                $user->is_active = 1;
                $user->save();
                Mail::to($user->email)->send(new ValidationOk($user));
            }
            //Guardo cambios hechos en user


            return redirect()->back();
        }
    }

    public function changeSelected(Request $request) {
        foreach ($request->get("selected") as $id){
            $user = User::findorfail($id);
            $this->changeActive($user);
        }

        return redirect()->back();
    }

    public function profile(User $user) {

        switch ($user->rol) {
            case 'is_admin':
                $teacher = $user->teacher;
                return view('teachers.show', compact('teacher'));
                break;

            case 'is_teacher':
                $teacher = $user->teacher;
                return view('teachers.show',compact('teacher'));
                break;

            case 'is_student':
                $student = $user->student;
                return view('student.show', compact('student'));
                break;

            case 'is_enterprise':
                $enterprise = $user->enterprise;
                return view('enterprise.show', compact('enterprise'));
                break;
            
            default:
                return route('home');
                break;
        }
    }
}
