<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfessionalFamilyStoreRequest;
use App\ProfessionalFamily;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ProfessionalFamilyUpdateRequest;

class ProfessionalFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profesionalfamilys = ProfessionalFamily::Name($request)->paginate(10);

        return view("family.index",compact("profesionalfamilys","request"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessionalFamilyStoreRequest $request)
    {
        $family = new ProfessionalFamily(["nombre" => $request->get("nombre")]);
        $family->save();
        Session::flash('message', 'Familia profesional agregada correctamente');
        return redirect()->route("family.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfessionalFamily  $professionalFamily
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalFamily $family)
    {
        //dd($family);
        return view("family.show",compact("family"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfessionalFamily  $family
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalFamily $family)
    {
        return view("family.edit",compact("family"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfessionalFamily  $family
     * @return \Illuminate\Http\Response
     */
    public function update(ProfessionalFamilyUpdateRequest $request, ProfessionalFamily $family)
    {
        //dd($family);
        $family->fill(["nombre" => $request->get("nombre")]);
        $family->save();
        Session::flash('message', 'Familia profesional editada correctamente');

        return redirect()->route("family.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfessionalFamily  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalFamily $family)
    {
        $family->delete();

        return redirect()->route("family.index");
    }
}
