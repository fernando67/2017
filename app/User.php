<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];



    /** RELACIONES **/ //Aquí las relaciones

    //relacion con Student
    public function student() {
        return $this->hasOne('App\Student');
    }

    //relacion con teacher
    public function teacher()
    {
        return $this->hasOne('App\Teacher');
    }

    //relacion con teacher
    public function validatedBy()
    {
        return $this->hasOne('App\TeacherValidation');
    }

    //relacion con Enterprise
    public function enterprise()
    {
        return $this->hasOne('App\Enterprise');
    }

    public function rol() {
        if($this->teacher) {
            if($this->teacher->is_admin){
                return "is_admin";
            }else {
                return "is_teacher";
            }
        }
        else if($this->student) {
            return "is_student";
        }
        else if($this->enterprise) {
            return "is_enterprise";
        }
    }

    public function role() {
        if($this->teacher) {
            if($this->teacher->is_admin){
                return "admin";
            }else {
                return "teacher";
            }
        }
        else if($this->student) {
            return "student";
        }
        else if($this->enterprise) {
            return "enterprise";
        }
    }

    public function getRolAttribute()
    {
        return $this->rol();
    }

    public function getRoleAttribute()
    {
        return $this->role();
    }




    /** SETTERS **/ //Aquí los setters

    //setter de Password de user
    public function setPasswordAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['password'] = bcrypt($value);
        }
    }










    /** SCOPES **/ //Aquí los scopes

    //Scope para el nombre de user
    public function scopeName($query,$name)
    {
        if (trim($name) != '') {
            $query->where('name','LIKE',"%$name%");
        }

    }

    //Scope para el telefono de user
    public function scopeTelefono($query,$telefono)
    {
        if (trim($telefono) != '') {
            $query->where('phone','LIKE',"%$telefono%");
        }
    }

    //Scope para el email de user
    public function scopeEmail($query,$email)
    {
        if (trim($email) != '') {
            $query->where('email','LIKE',"%$email%");
        }
    }

    //Scope para is_active de user
    public function scopeActive($query,$active)
    {
        if (trim($active) != '') {
            $query->where('is_active','=',$active);
        }
    }

    public function scopeRol($query, $id) {
        //Le pasamos el id de la tabla users, y nos devuelve un array de 2 índices, en la pos 0 el rol que es, y en la pos 1 el usuario y su relación 

        $q = $query->with('teacher', 'student', 'enterprise')->where('id', $id)->get();

        if($q[0]->teacher != null) {
            $rol = "is_teacher";
            $user = $q[0];
        }
        else if($q[0]->student != null) {
            $rol = "is_student";
            $user = $q[0];
        }
        else if($q[0]->enterprise != null) {
            $rol = "is_enterprise";
            $user = $q[0];
        }

        return [$rol, $user];
    }
}
