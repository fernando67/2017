<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable =[
        'requirements', 'recommended', 'title', 'work_day', 'schedule', 'contract', 'start_date', "description", "salary", "student_number"
    ];

    //Relacion con user
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function scopeDatos($query,$datos) {

        //Cambiar las que hagan falta
//me falta
        return $query->orWhere('title', 'like', "%".$datos."%")
            ->orWhere('salary', 'like', "%".$datos."%")
            ->orWhere('work_day', 'like', "%".$datos."%");

    }

}
